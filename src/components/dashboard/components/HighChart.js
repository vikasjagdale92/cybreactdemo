// ****************************************************************************
// ***************** Component : Alert Graph        ***************************
// ***************** Developer : Vikas Jagdale (06/05/2019)  ******************
// ****************************************************************************

import React,{Component} from 'react';
import Highcharts from 'highcharts';
import {graphData} from '../../../api/graphDataApi';

class HighChart extends Component{

    state={
      categories : [],
      chartType  : 'column'
    }

    componentDidMount(){
        const apiData = graphData(); 
          Highcharts.chart('container', {

            chart: {
              type: this.state.chartType
            },
          
            title: {
              text: 'Number of Alerts by Month'
            },
          
            xAxis: {
              categories: [...new Set(apiData.map(data=>data.monthYear))]
            },
          
            yAxis: {
              allowDecimals: false,
              min: 0,
              title: {
                text: ''

              }
            },
          
            tooltip: {
              formatter: function() {
                return '<b>' + this.x + '</b><br/>' +
                  this.series.name + ': ' + this.y + '<br/>' +
                  'Total: ' + this.point.stackTotal;
              }
            },
          
            plotOptions: {
              column: {
                stacking: 'normal'
              }
            },
          
            series: [
              {
              name: 'Low',
              data: apiData.filter((data)=> data.name==="Low").map(val=>val.range),
              color:' #3999D5'
            },
            {
              name: 'Medium',
              data: apiData.filter((data)=> data.name==="Medium").map(val=>val.range),
              color:'#ECD001'
            },
            {
              name: 'High',
              data: apiData.filter((data)=> data.name==="High").map(val=>val.range),
              color:'#E1150D'
              }
            ]
          });
    }

    //------------------ This method will execute when the chart type will change ---------------//
    componentDidUpdate(){
      const apiData = graphData(); 
          Highcharts.chart('container', {

            chart: {
              type: this.state.chartType
            },
          
            title: {
              text: 'Number of Alerts by Month'
            },
          
            xAxis: {
              categories: [...new Set(apiData.map(data=>data.monthYear))]
            },
          
            yAxis: {
              allowDecimals: false,
              min: 0,
              title: {
                text: ''

              }
            },
          
            tooltip: {
              formatter: function() {
                return '<b>' + this.x + '</b><br/>' +
                  this.series.name + ': ' + this.y + '<br/>' +
                  'Total: ' + this.point.stackTotal;
              }
            },
          
            plotOptions: {
              column: {
                stacking: 'normal'
              }
            },
          
            series: [
              {
              name: 'Low',
              data: apiData.filter((data)=> data.name==="Low").map(val=>val.range),
              color:' #3999D5'
            },
            {
              name: 'Medium',
              data: apiData.filter((data)=> data.name==="Medium").map(val=>val.range),
              color:'#ECD001'
            },
            {
              name: 'High',
              data: apiData.filter((data)=> data.name==="High").map(val=>val.range),
              color:'#E1150D'
              }
            ]
          });
    }

    getChartType =()=>{
      const chartType = document.getElementById('chartType').value;
      this.setState({
        chartType : chartType
      })
    }

    render(){
        var style  = {width:'100%', height:'350px'}
        return(
            <div className="col-lg-6 col-md-6 col-sm-6">
                <div className="col-lg-12 col-md-12 metric-alert-wrap">
                    <div className="col-lg-12 col-md-12 metric-alert-Tit">
                      Alerts
                      <select className="chartType pull-right" id="chartType" onChange={this.getChartType}>
                        <option value="column">Select chart type</option>                        
                        <option>column</option>
                        <option>spline</option>
                        <option>area</option>
                        <option>bar</option>
                        <option>pie</option>
                      </select>
                    </div>
                    <div id="container" style={style}>
                    </div>
                </div>
            </div>
        );
    }
}
export default HighChart;