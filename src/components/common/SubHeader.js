// ****************************************************************************
// ***************** Component : Sub Header        ***************************
// ***************** Developer : Vikas Jagdale (03/05/2019)  ******************
// ****************************************************************************

import React,{Component} from 'react';
import {metricApi} from '../../api/metricApi';

class SubHeader extends Component{
   
    constructor(props){
        super(props);
        this.state={
            showHideNav : false,
            metricApi   : '',
        }
    }
   
    componentDidMount(){
        this.setState ({
            metricApi : metricApi()
        });
    }

    getYears =(e)=>{
        const year = document.getElementById('years').value;
        this.props.getYear(year);
    }

    clearFilter =()=>{
        this.props.getYear(new Date().getFullYear()-1+'-'+ new Date().getFullYear());
    }

    render(){   
        const metricOverviewArr = this.state.metricApi;
        const unique = metricOverviewArr ? [...new Set(metricOverviewArr.map(data => data.year))] : [] ;
        if(unique.length){
            return(
                <div>
                    <ul className="nav nav-pills nav-sub-header ">
                        <li className="nav-item">
                            <a className="nav-link active" href="/">FORECAST</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/">OPTIMIZATION</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/">FARES</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link disabled" href="/">AVAILABILITY</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link disabled" href="/">CONFIGURATION</a>
                        </li>
                    </ul>
                    <div className="apply-filter-wrap">
                        <button type="button" className="btn btn-default btn-clear" onClick={this.clearFilter} >Clear</button>
                        <button type="button" className="btn btn-default" data-toggle="modal" data-target="#myModal">Apply Filter</button>
                        <div className="modal fade" id="myModal" role="dialog">
                            <div className="modal-dialog modal-sm">
                            <div className="modal-content">
                                <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal">&times;</button>
                                <h4 className="modal-title">Select Years</h4>
                                </div>
                                <div className="modal-body">
                                    <select className="form-control" onChange={this.getYears} id="years">
                                        {unique.map((years,index)=>{
                                            return <option key={index}>{years}</option>
                                        })}  
                                    </select>
                                </div>
                                <div className="modal-footer">
                                {/* <button type="button" className="btn btn-default" data-dismiss="modal">Close</button> */}
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
            }else{
                return(
                    <div>Loading...</div>
                );
            }
    }
}
export default SubHeader;